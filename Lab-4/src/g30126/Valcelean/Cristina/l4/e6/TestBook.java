package g30126.Valcelean.Cristina.l4.e6;

import g30126.Valcelean.Cristina.l4.e4.Author;
import g30126.Valcelean.Cristina.l4.e5.Book;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestBook {
    @Test
    public void testToString() {
        Author a1 = new Author("George Calinescu", "g_calin@yahoo.com", 'm');
        Author a2 = new Author("Popa Georgiana", "popa_georgi@yahoo.com", 'f');
        Author[] authors = new Author[2];
        authors[0] = a1;
        authors[1] = a2;
        g30126.Valcelean.Cristina.l4.e6.Book b = new g30126.Valcelean.Cristina.l4.e6.Book("Otilia", authors, 45);
        assertEquals(b.toString(), "Book Otilia by 2 authors");
    }

    @Test
    public void testprintAuthors() {
        Author a1 = new Author("George Calinescu", "g_calin@yahoo.com", 'm');
        Author a2 = new Author("Popa Georgiana", "popa_georgi@yahoo.com", 'f');
        Author a3 = new Author("Ilinca Gheorgehe", "ilincaaa93@yahoo.com", 'f');
        Author[] authors = new Author[3];
        authors[0] = a1;
        authors[1] = a2;
        authors[2] = a3;
        g30126.Valcelean.Cristina.l4.e6.Book b = new  g30126.Valcelean.Cristina.l4.e6.Book("Padurea Neagra", authors, 45);
        assertEquals(b.printAuthors(),"George Calinescu Popa Georgiana Ilinca Gheorgehe ");
    }

}