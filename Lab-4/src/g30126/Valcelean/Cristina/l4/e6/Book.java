package g30126.Valcelean.Cristina.l4.e6;

import g30126.Valcelean.Cristina.l4.e4.Author;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    public Book(String name, Author[] authors, double price){
        this.name=name;
        this.authors=authors;
        this.price=price;
        qtyInStock=0;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock){
        this.name=name;
        this.authors=authors;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthor() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        return "Book " + this.getName() + " by "+this.authors.length+ " authors";
    }

    public String printAuthors(){
        String s=new String();
        for(int i=0;i<this.authors.length;i++)
            s = s + this.authors[i].getName()+" ";
        return s;
    }
}
