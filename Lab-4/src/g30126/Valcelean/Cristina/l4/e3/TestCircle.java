package g30126.Valcelean.Cristina.l4.e3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCircle {
    @Test
    public void testArea(){
        Circle c = new Circle(2);
        assertEquals(c.getArea(),12.566370614359172, 0.01);
    }

    @Test
    public void testRadius() {
        Circle c = new Circle(2);
        assertEquals(c.getRadius(), 2, 0.01);
    }
}
