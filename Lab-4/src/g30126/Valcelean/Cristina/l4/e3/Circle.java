package g30126.Valcelean.Cristina.l4.e3;

public class Circle {

    public double radius;
    private String color;

    public Circle(){
       this.color="red";
       this.radius=1.0;
    }

    public Circle(double radius) {
        this.radius=radius;
        this.color="red";
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return radius*radius*Math.PI;
    }
}
