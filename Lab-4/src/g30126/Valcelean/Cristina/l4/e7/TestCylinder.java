package g30126.Valcelean.Cristina.l4.e7;

import g30126.Valcelean.Cristina.l4.e3.Circle;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCylinder {
    @Test
    public void testArea() {
        Circle c = new Cylinder(3,2);
        assertEquals(c.getArea(),94.24777960769379,0.01 );
    }
    @Test
    public void testVolume() {
        Cylinder c = new Cylinder(3,2);
        assertEquals(c.getVolume(),56.548667764616276,0.01 );
    }
}
