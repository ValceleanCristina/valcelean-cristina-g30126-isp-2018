package g30126.Valcelean.Cristina.l4.e7;

import g30126.Valcelean.Cristina.l4.e3.Circle;

public class Cylinder extends Circle{
    private double height;

    public Cylinder(){
        super();
        System.out.println("Object created!");
    }

    public Cylinder(double radius){
        super(radius);
        height=1.0;
    }

    public Cylinder(double radius, double height){
        super(radius);
        this.height=height;
    }

    public double getHeight() {
        return height;
    }

    public  double getVolume(){
        return super.getRadius()*super.getRadius()*height*Math.PI;
    }
    @Override
    public double getArea(){
        return 2*Math.PI*super.getRadius()*(super.getRadius()+this.height);
    }
}
