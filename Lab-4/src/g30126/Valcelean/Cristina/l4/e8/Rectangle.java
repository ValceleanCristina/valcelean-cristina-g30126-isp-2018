package g30126.Valcelean.Cristina.l4.e8;

public class Rectangle extends Shape{
    private double width;
    private double length;

    public Rectangle(){
        width=1.0;
        length=1.0;
    }

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length, String color, boolean filled) {
        this.width = width;
        this.length = length;
        super.setColor(color);
        super.setFilled(filled);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return length*width;
    }

    public double getPerimeter(){
        return 2*(length+width);
    }

    @Override
    public String toString() {
        return ("A rectangle with width="+this.getWidth()+" and length="+this.getLength()+", which is a subclass of "+super.toString());
    }
}
