package g30126.Valcelean.Cristina.l4.e8;

public class Shape {
    String color;
    Boolean filled;

    public Shape() {
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = "red";
        this.filled = true;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(Boolean filled) {
        this.filled = filled;
    }

    public boolean FILLED() {
        return this.filled;
    }

    public boolean isFilled() {
        if (this.FILLED() == true)
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        if (this.isFilled())
            return ("A shape with color of " + this.getColor() + " and filled");
        else
            return ("Not filled");
    }
}

