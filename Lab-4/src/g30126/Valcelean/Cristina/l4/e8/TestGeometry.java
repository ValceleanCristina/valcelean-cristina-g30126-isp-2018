package g30126.Valcelean.Cristina.l4.e8;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestGeometry {
    @Test
    public void testShapeToString() {
        Shape s = new Shape("red", true);
        assertEquals(s.toString(), "A shape with color of red and filled");
    }

    @Test
    public void testCircleArea() {
        Circle c = new Circle(3);
        assertEquals(c.getArea(), 28.274333882308138, 0.01);
    }

    @Test
    public void testCirclePerimeter() {
        Circle c = new Circle(3);
        assertEquals(c.getPerimeter(), 18.84955592153876, 0.01);
    }

    @Test
    public void testCircleToString() {
        Circle c = new Circle(3);
        assertEquals(c.toString(), "A circle with radius=3.0, which is a subclass of A shape with color of green and filled");
    }

    @Test
    public void testRectangleArea() {
        Rectangle r = new Rectangle(3,2);
        assertEquals(r.getArea(), 6, 0.01);
    }

    @Test
    public void testRectanglePerimeter() {
        Rectangle r = new Rectangle(3,2);
        assertEquals(r.getPerimeter(), 10, 0.01);
    }

    @Test
    public void testRectangleToString() {
        Rectangle r = new Rectangle(3,2);
        assertEquals(r.toString(),"A rectangle with width=3.0 and length=2.0, which is a subclass of A shape with color of green and filled");
    }

    @Test
    public void testSquareArea() {
        Square r = new Square(3);
        assertEquals(r.getArea(), 9, 0.01);
    }

    @Test
    public void testSquarePerimeter() {
        Square r = new Square(3);
        assertEquals(r.getPerimeter(), 12, 0.01);
    }

    @Test
    public void testSquareToString() {
        Square r = new Square(3);
        assertEquals(r.toString(), "A Square with side 3.0, which is a subcalss ofA rectangle with width=3.0 and length=3.0, which is a subclass of A shape with color of green and filled");
    }

}

