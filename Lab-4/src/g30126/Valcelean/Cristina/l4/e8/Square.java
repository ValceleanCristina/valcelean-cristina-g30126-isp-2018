package g30126.Valcelean.Cristina.l4.e8;

public class Square extends  Rectangle{

    public Square(){
        super();
        System.out.println("Object created!");
    }
    public Square(double side){
        super(side,side);
    }

    public Square(double side, String color, boolean filled){
        super(side,side,color,filled);
    }

    public double getSide(){
        return super.getLength();
    }

    public void setSide(double side){
        super.setLength(side);
        super.setWidth(side);
    }

    public void setWidth(double side){
        super.setWidth(side);
        super.setLength(side);
    }

    public void setLength(double side){
        super.setWidth(side);
        super.setLength(side);
    }

    @Override
    public String toString() {
        return ("A Square with side "+this.getSide()+", which is a subcalss of"+super.toString());
    }
}

