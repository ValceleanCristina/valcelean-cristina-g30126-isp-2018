package g30126.Valcelean.Cristina.l4.e9;

import becker.robots.*;

public class Diver {

        private int x;
        private int y;
        private Robot Karel;
        private City NY;

        public Diver(int x, int y) {
            this.x = x;
            this.y = y;

            this.NY = new City();
            this.Karel = new Robot(NY,x,y,Direction.NORTH);

            Wall w0 = new Wall(NY,x+1,y,Direction.NORTH);
            Wall w1 = new Wall(NY,x+1,y-1,Direction.EAST);
            Wall w2 = new Wall(NY,x+2,y-1,Direction.EAST);
            Wall w3 = new Wall(NY,x+3,y-1,Direction.EAST);
            Wall w4 = new Wall(NY,x+3,y,Direction.WEST);
            Wall w5 = new Wall(NY,x+3,y,Direction.SOUTH);
            Wall w6 = new Wall(NY,x+3,y+1,Direction.SOUTH);
            Wall w7 = new Wall(NY,x+3,y+2,Direction.WEST);
        }

        public void Dive() {
            Karel.move();
            Karel.turnLeft();
            Karel.turnLeft();
            Karel.turnLeft();
            Karel.move();
            Karel.turnLeft();
            Karel.turnLeft();
            Karel.turnLeft();
            Karel.move();
            Karel.move();
            Karel.move();
            Karel.move();
            Karel.turnLeft();
            Karel.turnLeft();
        }
        public static void main(String[] args) {
            Diver Karel = new Diver(2,2);
            Karel.Dive();
        }
    }


