package g30126.Valcelean.Cristina.l4.e5;

import g30126.Valcelean.Cristina.l4.e4.Author;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class testBook {
    @Test
    public void testToString() {
        Author a=new Author("George Calinescu","g_calin@yahoo.com",'m');
        Book b = new Book("Otilia", a, 45);
        assertEquals(b.toString(), "Book Otilia by author George Calinescu (m) at g_calin@yahoo.com");
    }
    @Test
    public void testPrice(){
        Author a=new Author("Bora Simona","simonaaa@yahoo.com",'f');
        Book b = new Book("Ploaia diminetii",a, 45);
        assertEquals(b.getPrice(),45,0.01);
    }
    @Test
    public void testQtyInStock(){
        Author a=new Author("Lazar Popescu","lazar_popescu@yahoo.com",'m');
        Book b = new Book("Noaptea neagra", a, 45,5);
        assertEquals(b.getQtyInStock(),5,0.01);
    }
}
