package g30126.Valcelean.Cristina.l4.e4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestAuthor {
    @Test
    public void testGender(){
        Author a=new Author("Pop Teodor","teodor_pop@yahoo.com",'m');
        assertEquals(a.getGender(),'m');
    }
    @Test
    public void testToString(){
        Author a=new Author("Lavinia Pop","lav_pop@yahoo.com",'f');
        assertEquals(a.toString(),"Author Lavinia Pop (f) at lav_pop@yahoo.com");
    }
    @Test
    public void testEmail(){
        Author a=new Author("Georgiana Andreica","georgi_andreica@yahoo.com",'f');
        assertEquals(a.getEmail(),"georgi_andreica@yahoo.com");
    }
}
