package g30126.Valcelean.Cristina.l4.e2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMyPoint {
    @Test
    public void testToString() {
        MyPoint p = new MyPoint(2,3);
        assertEquals(p.toString(), "(2,3)");
    }
    @Test
    public void testDistance() {
        MyPoint p = new MyPoint(2,3);
        assertEquals(p.distance(3,8), 5.0990195135927845,0.01);
    }
    @Test
    public void testOverloadDistance() {
        MyPoint p1 = new MyPoint(1,3);
        MyPoint p2 = new MyPoint(4,6);
        assertEquals(p1.OverloadDistance(p2), 4.242640687119285,0.01);
    }

}
