package g30126.Valcelean.Cristina.l2.e3;

import java.util.Scanner;

public class Ex3 {
       static int prime(int nr) {
           int i, d = 0;
           for (i = 1; i<= nr; i++)
               if (nr % i == 0) {
                   d++;
               }
           if (d == 2) {
               return 1;
           } else {
               return 0;
           }
       }
       static void f1(int A,int B,int nr)
       {
           int i;
           nr=0;
           for(i=A;i<=B;i++)
               if(prime(i)==1)
               {
                   nr++;
                   System.out.println(i);
               }
               System.out.println("Prime numbers:" +nr);
       }
       public static void main (String[] args)
       {
           Scanner in= new Scanner(System.in);
           int A,B,nr=0;
           A=in.nextInt();
           B=in.nextInt();
           System.out.println("The prime numbers are:");
           f1(A,B,nr);
       }
}