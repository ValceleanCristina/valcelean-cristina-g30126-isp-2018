package g30126.Valcelean.Cristina.l2.e5;

import java.util.Random;

public class Ex5 {
    static void bubbleSort(int[] v) {
        int n = v.length;
        int aux = 0;
        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){
                if(v[j-1] > v[j]){
                    aux = v[j-1];
                    v[j-1] = v[j];
                    v[j] = aux;
                }

            }
        }
    }
    public static void main(String[] args) {
        Random r = new Random();
        int[] v = new int[10];
        for(int i=0;i<v.length;i++){
            v[i] = r.nextInt(100);
        }
        System.out.println("Before sorting");
        for (int i: v){
            System.out.print(i+ " ");
        }
        System.out.println();
        bubbleSort(v);
        System.out.println("After sorting");
        for(int i : v){
            System.out.print(i+ " ");
        }
    }
}


