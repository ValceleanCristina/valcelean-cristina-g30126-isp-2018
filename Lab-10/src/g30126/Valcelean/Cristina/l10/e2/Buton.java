package g30126.Valcelean.Cristina.l10.e2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Buton implements ActionListener {

    JButton Buton = new JButton("Buton");
    JFrame a = new JFrame();
    JTextField Afisare = new JTextField(15);

    int i = 0;

    public Buton(){

        Afisare.setText("Click-uri:"+i);
        a.setTitle("Buton");
        a.setVisible(true);
        a.setSize(300,300);
        a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        a.setResizable(true);

        a.setLayout(new FlowLayout());
        a.add(Afisare);
        a.add(Buton);

        Buton.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() == Buton) {
            i++;
            Afisare.setText("Click-uri:" +i);
        }
    }
    public static void main(String args[]){
        new Buton();
    }
}
