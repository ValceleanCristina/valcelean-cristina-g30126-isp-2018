package g30126.Valcelean.Cristina.l8.e1;

import java.util.Objects;

public class BankAccount {

        public String owner;
        public double balance;

        public BankAccount(String owner, double balance)
        {
            this.balance=balance;
            this.owner=owner;
        }

        public void withdraw( double amount) {
            if(this.balance-amount>0){
                this.balance-=amount;
            }
            else{
                System.out.println("Unsuccessful withdrawal! Insufficient amount!");
            }
        }

        public void deposit(double amount) {

            balance+=amount;
        }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 &&
                Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {

        return Objects.hash(owner, balance);
    }

        public static void main(String[] args) {
            BankAccount b1 = new BankAccount("Alin",200);
            BankAccount b2 = new BankAccount("Dora",300);
            BankAccount b3 = new BankAccount("Alina",200);

            b1.withdraw(30);
            b2.withdraw(500);

            //amount verification
            if(b1.equals(b2))
                System.out.println(b1+" and "+b2+ " are equals!");
            else
                System.out.println(b1+" and "+b2+ " are NOT equals!");
            if(b1.equals(b3))
                System.out.println(b1+" and "+b3+ " are equals!");
            else
                System.out.println(b1+" and "+b3+ " are NOT equals!");

            //owner verification
            if(b1.owner.equals(b2.owner))
                System.out.println(b1+" and "+b2+" have the same owners");
            else
                System.out.println(b1+" and "+b2+" have different owners");

            b1.deposit(100);
            b2.deposit(50);

            //hashCode verification
            if (b1.hashCode() == b2.hashCode()) {
                System.out.println("HashCode equals!");
            }
            else{
                System.out.println("Hashcode not equals!");
            }
            if (b1.hashCode() == b3.hashCode()) {
                System.out.println("HashCode equals!");
            }
            else{
                System.out.println("Hashcode not equals!");
            }

        }
 }
