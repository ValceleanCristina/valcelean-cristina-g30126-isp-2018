package g30126.Valcelean.Cristina.l8.e4;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static java.lang.System.exit;

public class Main {
    public static void main(String[] args) throws Exception {
        Dictionary d = new Dictionary();
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
        String word,definition;
        int option;
//        People-Human beings in general or considered collectively
//        Processo-A machine that processes something
//        Book-A written or printed work consisting of pages glued or sewn together along one side and bound in covers

        do {
            System.out.println("Menu");
            System.out.println("1. Add word");
            System.out.println("2. Find the definion of a word");
            System.out.println("3. Display all definitions ");
            System.out.println("4. Display all words ");
            System.out.println("5. Exit");

            word = fluxIn.readLine();
            option = word.charAt(0);

            switch (option) {
                case 1:
                    System.out.println("Enter the word:");
                    word = fluxIn.readLine();
                    if (word.length() > 1) {
                        System.out.println("Enter the definition:");
                        definition = fluxIn.readLine();
                        d.addWord(new Word(word), new Definition(definition));
                    }
                    break;
                case 2:
                    System.out.println("The wanted word:");
                    word = fluxIn.readLine();
                    if (word.length() > 1) {
                        Definition def =new Definition();
                        def = d.getDefinition(new Word(word));
                        if (def == null)
                            System.out.println("Does not exist!");
                        else
                            System.out.println("Definition:" + def);
                    }
                    break;
                case 3:
                    System.out.println("Display all definition:");
                    d.getAllDefinition();
                    break;
                case 4:
                    System.out.println("Display all the words:");
                    d.getAllWords();
                    break;
                case 5:
                    exit(0);
            }

        }while(option<=5 );
    }
}