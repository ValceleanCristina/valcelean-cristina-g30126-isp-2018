package g30126.Valcelean.Cristina.l8.e4;

import java.util.Iterator;
import java.util.HashMap;

public class Dictionary {
    private HashMap hashMap = new HashMap();

    public Dictionary(){}

    public void addWord(Word w,Definition d){
        if(!hashMap.containsKey(w)){
            System.out.println("Adding word!");
            hashMap.put(w,d);
        }
        else
        {
            System.out.println("The word already exist!");
            hashMap.put(w,d);
        }
    }

    public Definition getDefinition(Word w){
        if(hashMap.containsKey(w)){
            Definition definition = (Definition) hashMap.get(w);
            return definition;
        }
        System.out.println("The word wasn't found!");
        return null;
    }

    public void getAllWords(){
        System.out.println("\nDispaly all the words \n");
        Iterator iterator = hashMap.keySet().iterator();
        while(iterator.hasNext())
            System.out.println(iterator.next());
    }

    public void getAllDefinition(){
        System.out.println("\nDisplay all the definitions \n");
        for (Word word : (Iterable<Word>) hashMap.keySet()) {
            Definition def = getDefinition(word);
            System.out.println(def);
        }
    }
}
