package g30126.Valcelean.Cristina.l8.e3;

import java.util.Comparator;

public class BankAccountsCompare implements Comparator<BankAccount> {

    @Override
    public int compare(BankAccount o1, BankAccount o2) {
		String o1Owner = o1.getOwner().toUpperCase();
        String o2Owner = o2.getOwner().toUpperCase();
        return o1Owner.compareTo(o2Owner);
    }
}
