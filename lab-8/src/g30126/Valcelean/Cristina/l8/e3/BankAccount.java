package g30126.Valcelean.Cristina.l8.e3;

import java.util.Objects;

public class BankAccount implements Comparable<BankAccount>{
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw( double amount) {
        if(this.balance-amount>0){
            this.balance-=amount;
        }
        else{
            System.out.println("Unsuccessful withdrawal! Insufficient amount!");
        }
    }

    public void deposit(double amount) {
        balance += amount;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    @Override
    public int compareTo(BankAccount bankAccount) {
        return (int) (balance - bankAccount.balance);
    }

    public String toString() {
        return "BankAccount{" + "owner='" + owner + '\'' + ", balance=" + balance + '}';
    }

}
