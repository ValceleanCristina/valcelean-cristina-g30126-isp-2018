package g30126.Valcelean.Cristina.l8.e3;

import java.util.*;

public class Bank {
    private TreeSet<BankAccount> accounts = new TreeSet<>() ;
    private TreeSet<BankAccount> accountsSorted = new TreeSet<>(new BankAccountsCompare()) ;

    public void addAccount(String owner, double balance) {
        accounts.add(new BankAccount(owner, balance));
        accountsSorted.add(new BankAccount(owner,balance));
    }

    public void printAccounts() {
        System.out.println(accounts);
    }

    public void printAccounts(double minBalance, double maxBalance) {
        //System.out.println("Printing accounts between " +minBalance + " and " +maxBalance);
        System.out.println(accounts.subSet(new BankAccount("x",minBalance),new BankAccount("y",maxBalance)));
    }

    public BankAccount getAccount(String owner)
    {
        Iterator<BankAccount> iterator = accounts.iterator();
        while (iterator.hasNext()) {
            BankAccount a = iterator.next();
            if(a.getOwner().equals(owner))
            {
                System.out.println("The account is: " +a.getOwner()+" "+a.getBalance());
                return a;
            }
        }
        System.out.println("Account doesn't found!");
        return null;
    }

    public void getAllAccounts(){
        System.out.println(accountsSorted);
    }



}
