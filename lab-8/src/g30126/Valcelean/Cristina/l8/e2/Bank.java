package g30126.Valcelean.Cristina.l8.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Bank {

    private ArrayList<BankAccount> accounts = new ArrayList<>() ;

    public void addAccount(String owner, double balance) {
        accounts.add(new BankAccount(owner, balance));
    }

     public void printAccounts() {
         Collections.sort(accounts);
         for(int i=0; i<accounts.size(); i++){
             System.out.println("Owner " +accounts.get(i).getOwner() + " ,balance=" +accounts.get(i).getBalance());
         }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        ArrayList <BankAccount> accountsbetween = new ArrayList<BankAccount>();
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getBalance() < maxBalance && accounts.get(i).getBalance() > minBalance) {
                accountsbetween.add(accounts.get(i));
            }
        }
        for (int i=0; i<accountsbetween.size();i++){
            System.out.println(accountsbetween.get(i).toString());
        }
    }

    public BankAccount getAccount(String owner){
        for(int i=0; i<accounts.size(); i++){
            if(accounts.get(i).getOwner().equals(owner))
                return accounts.get(i);
        }
        return null;
    }

    //comparator by balance (int)
    //double balance1=o1.getBalance();
    //double balance2=o2.getBalance();
    //return (int) (balance1-balance2);

    public static Comparator<BankAccount> comparatorOwner= (o1, o2) -> {
        String owner1 = o1.getOwner().toUpperCase();
        String owner2 = o2.getOwner().toUpperCase();

        return owner1.compareTo(owner2);

    };

    public void getAllAccounts(){
        Collections.sort(accounts,comparatorOwner);
        for(int i=0; i<accounts.size(); i++){
            System.out.println("Owner " +accounts.get(i).getOwner() + " ,balance=" +accounts.get(i).getBalance());
        }
    }


}