package g30126.Valcelean.Cristina.l8.e2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();

        bank.addAccount("Ana", 100);
        bank.addAccount("Dora", 80);
        bank.addAccount("George", 300);
        bank.addAccount("Paul", 400);
        bank.addAccount("Dan", 150);
        System.out.println("\nA sorted list (by balance) of all accounts!\n");
        bank.printAccounts();
        System.out.println("\nAll accounts between min range and max range values!\n");
        bank.printAccounts(90, 300);
        System.out.println("\nA sorted list alphabetically by owner field!\n");
        bank.getAllAccounts();
        System.out.println("\nThe wanted owner is: ");
        System.out.println(bank.getAccount("Dan"));
    }
}
