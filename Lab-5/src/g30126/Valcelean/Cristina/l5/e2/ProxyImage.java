package g30126.Valcelean.Cristina.l5.e2;

public class ProxyImage implements Image{

    private String fileName;
    private Image image;
    private Boolean choice;
    public ProxyImage(String fileName,Boolean choice)
    {
        this.fileName = fileName;
        this.choice=choice;
    }
    @Override
    public void display()
    {
        if(choice == true)
        {
            image = new RotatedImage(fileName);
        }
        else
        {
            image = new  RealImage(fileName);
        }
        image.display();
    }
    public static void main (String[] args)
    {
        ProxyImage p = new ProxyImage("Robot",true);
        p.display();

    }
}