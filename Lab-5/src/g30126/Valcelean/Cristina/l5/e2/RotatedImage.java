package g30126.Valcelean.Cristina.l5.e2;

public class RotatedImage implements Image{

    private String fileName;
    public RotatedImage (String fileName)
    {
        this.fileName=fileName;
    }
    public void display()
    {
        System.out.println("Display rotated " + fileName );
    }

}
