package g30126.Valcelean.Cristina.l5.e1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestGeometry {

    @Test
    public void testgetArea() {
        Shape s[] = new Shape[4];
        s[0] = new Circle(2);
        s[1] = new Rectangle(3, 2);
        s[2] = new Square(4);

        double[] areas = new double[3];
        areas[0] = 12.566370614359172;
        areas[1] = 6.0;
        areas[2] = 16.0;
        for (int i = 0; i < 3; i++) {
            assertEquals(areas[i], s[i].getArea(), 0.01);
        }
    }

    @Test
    public void testgetPerimeter() {
        Shape s[]=new Shape[4];
        s[0] = new Circle(2);
        s[1] = new Rectangle(3,2);
        s[2] = new Square(4);

        double[] areas = new double[3];
        areas[0]  =12.566370614359172;
        areas[1]  =10.0;
        areas[2]  =16.0;
        for(int i=0;i<3;i++){
            assertEquals(areas[i], s[i].getPerimeter(),0.01);
        }
    }
}
