package g30126.Valcelean.Cristina.l5.e3;

public class Controller {

    public TemperatureSensor tempSensor;
    public LightSensor lightSensor;

    //Exercise 4 - Update the Controller class from exercise 3 to be implemented as a Singleton.

    private static Controller controller;

    private Controller() {

    }
    public static Controller getControl() {
        if(controller == null)
            controller = new Controller();
        return controller;
    }

    //

    public void control() {
        int i;
        for( i=0;i<20;i++)
        {
            tempSensor=new TemperatureSensor();
            lightSensor= new LightSensor();
            System.out.println(tempSensor.readValue());
            System.out.println(lightSensor.readValue());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
    public static void main (String[] args) {
        Controller c = getControl();
        c.control();
    }
}
