package g30126.Valcelean.Cristina.l5.e3;

public class LightSensor extends Sensor {

    int valueSensor;

    public LightSensor() {
        this.valueSensor=(int)(Math.random()*100);
    }

    public	int readValue() {
        return this.valueSensor;
    }
}