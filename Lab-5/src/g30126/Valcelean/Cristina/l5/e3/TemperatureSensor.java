package g30126.Valcelean.Cristina.l5.e3;

public class TemperatureSensor extends Sensor {

    int valueTemp;

    public TemperatureSensor() {
        this.valueTemp=(int)(Math.random()*100);
    }

    public int readValue() {
        return this.valueTemp;
    }
}
