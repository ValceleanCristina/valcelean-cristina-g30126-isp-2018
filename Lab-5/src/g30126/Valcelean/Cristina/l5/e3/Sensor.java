package g30126.Valcelean.Cristina.l5.e3;

public abstract class Sensor {

    private String location;

    public abstract int readValue();

    public String getLocation() {
        return this.location;
    }
    public static void main(String[] args) { }

}
