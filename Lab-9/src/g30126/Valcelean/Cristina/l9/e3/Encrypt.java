package g30126.Valcelean.Cristina.l9.e3;

import java.io.*;
import java.util.Scanner;

public class Encrypt {
    private String fileName,operation;

    public Encrypt() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the operation encrypt/decrypt: " );
        operation = in.next();
        System.out.println("Enter the filnename: " );
        fileName = in.next();
    }

    public String getOperation() {
        return operation;
    }

    public void encryptFile() throws IOException {
        BufferedReader r = new BufferedReader(new FileReader(fileName)); // fisier de citire
        String s, s1 = new String();
        while ((s = r.readLine()) != null) {
            s1 += s +"\n"; //adauga fiecare rand la sir
            System.out.println(s1); //afiseaza in consola sirul dupa fiecare pas
        }
        r.close();
        BufferedWriter w = new BufferedWriter(new FileWriter("data.enc")); //fisierul pentru scriere
        String s2 = new String();
        for (int i = 0; i < s1.length(); i++) { //parcurgem sirul
            int a = s1.charAt(i); //charAt(i) returneaza caracterul de la indexul i si e convertit in int -> cod ascii
            a--;              //siftam cu 1 la stanga
            char b = (char) a; //convestete a-ul iar in char si ii atribuie variabilei b
            s2 += b; //se adauga la sir caracterul din variabila b
        }
        w.write(s2);
        w.close();
    }

    public void decryptFile() throws IOException {
        BufferedReader r = new BufferedReader(new FileReader(fileName));
        String s, s1 = new String();
        while ((s = r.readLine()) != null) {
            s1 += s +  "\n";
            System.out.println(s1);
        }
        r.close();
        BufferedWriter w = new BufferedWriter(new FileWriter("text.dec"));
        String s2 = new String();
        for (int i = 0; i < s1.length(); i++) {
            int a = s1.charAt(i);
            a++;
            char b = (char) a;
            s2 += b;
        }
        w.write(s2);
        w.close();
    }
}

