package g30126.Valcelean.Cristina.l9.e3;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Encrypt e = new Encrypt();
        String s = new String();
        s = e.getOperation();
        if (s.equals("encrypt"))
            e.encryptFile();
        else if (s.equals("decrypt"))
            e.decryptFile();
    }
}
