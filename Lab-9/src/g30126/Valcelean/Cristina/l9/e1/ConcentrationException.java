package g30126.Valcelean.Cristina.l9.e1;

public class ConcentrationException extends Exception {
      int c;
      public ConcentrationException(int c,String msg) {
            super(msg);
            this.c = c;
      }
      int getConc(){
          return c;
        }
}
