package g30126.Valcelean.Cristina.l9.e2;

import java.io.FileReader;
import java.io.IOException;


public class Main {
        public static void main(String args[]) {
            int nr=0;
            try{
                FileReader in = new FileReader("D://data.txt");  //crearea unui flux de intrare pe caractere
                char [] a = new char[50];
                in.read(a);   // citeste continutul array-ului
                for(char c : a)
                    if (c=='e')
                        nr++;
                System.out.print("The number of times character 'e' appears in the file is: "+nr);
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
}