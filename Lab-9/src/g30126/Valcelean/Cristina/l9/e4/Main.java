package g30126.Valcelean.Cristina.l9.e4;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
       Car c1 = new Car("Opel",5000);
       Car c2 = new Car("Audi",2000);
       Car c3 = new Car("Tesla",10000);
       c1.saveCar("carOpel");
       c2.saveCar("carAudi");
       c3.saveCar("carTesla");

      try {
           Car co = Car.load("carOpel");
           System.out.println(co);
          Car ca = Car.load("carAudi");
          System.out.println(ca);
          Car ct = Car.load("carTesla");
          System.out.println(ct);
       } catch (IOException e) {
            e.printStackTrace();
       } catch (ClassNotFoundException e) {
           e.printStackTrace();
        }

    }
}
