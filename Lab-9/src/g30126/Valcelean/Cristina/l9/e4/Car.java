package g30126.Valcelean.Cristina.l9.e4;

import java.io.*;

public class Car implements Serializable{
    private int price;
    private String model;

    public Car(String model,int price ) {
        this.price = price;
        this.model = model;
    }

    @Override
    public String toString() {
        return "Car{" + "price=" + price + ", model='" + model + '\'' + '}';
    }

    //add car
    public Car addCar(String name,int price){
        Car c = new Car(name,price);
        System.out.println(c+" is create");
        return c;
    }
    //save car
    void saveCar(String fileName) {
        try {
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Car saved to folder.");
        } catch (IOException e) {
            System.err.println("The car can't be written to the file.");
            e.printStackTrace();
        }
    }
    //load car
    static Car load(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream in =
                new ObjectInputStream(
                        new FileInputStream(fileName));
        Car c = (Car)in.readObject();
        return c;
    }


}
