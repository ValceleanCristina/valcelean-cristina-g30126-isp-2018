package g30126.Valcelean.Cristina.l11.e3;

public class Counters extends Thread {
    String n;
    Thread t;

    Counters(String n, Thread t){
        this.n = n;
        this.t=t;
    }

    public void run()
    {
        System.out.println("Firul "+n+" a intrat in metoda run()");
        try
        {
            if (t!=null) {
                t.join();

                for(int i=100; i<200; i++){
                    try{
                        System.out.println(n+ " este la pozitia : " +(i+1));
                        Thread.sleep(100);
                    } catch(Exception e){
                        e.printStackTrace();}
                }
            }

            else{

                for(int i=0; i<100; i++){
                    try{
                        System.out.println(n+ " este la pozitia : " +(i+1));
                        Thread.sleep(200);
                    } catch(Exception e){
                        e.printStackTrace();}
                }

            }

        }
        catch(Exception e){
            e.printStackTrace();}

    }

    public static void main(String[] args)
    {
        Counters w1 = new Counters("Proces 1",null);
        Counters w2 = new Counters("Proces 2",w1);
        w1.start();
        w2.start();
    }
}
