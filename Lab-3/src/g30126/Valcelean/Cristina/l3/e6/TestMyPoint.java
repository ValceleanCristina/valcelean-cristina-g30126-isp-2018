package g30126.Valcelean.Cristina.l3.e6;

public class TestMyPoint {
    public static void main(String[] args) {

        MyPoint p1 = new MyPoint(); //constructs and initializes a point at the origin (0, 0)
        System.out.println(" ");
        MyPoint p2 = new MyPoint(2,3);

        //displays the points coordinates
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(" ");

        //sets the coordinates for p1
        p1.setX(5);
        p1.setY(5);
        System.out.println(p1);
        System.out.println(" ");

        //sets the coordinates for p1 using the method for both x and y
        p1.setXY(4,6);
        System.out.println(p1);
        System.out.println(" ");

        //prints the coordinates using toString method
        System.out.println(p1.toString());
        System.out.println(" ");

        //prints the distance from this point to the point at the given (x, y) coordinates
        System.out.println("The distance between the 2 point is:");
        System.out.println(p1.distance(2,3));

        //prints the distance  between this point and a given MyPoint instance
        System.out.println("The distance between this point and a given MyPoint instance:");
        System.out.println(p1.OverloadDistance(p2));
    }
}

