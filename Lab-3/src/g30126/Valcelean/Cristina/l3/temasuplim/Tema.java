package g30126.Valcelean.Cristina.l3.temasuplim;

public class Tema {
    public static int solution(int[] A) {
        boolean sol = true;
        //The following lines goes through a vector and finds the unknown element
        for(int i = 0; i < A.length; i++){
            sol = true;
            for(int j = 0; j < A.length; j++)
                if(A[i] == A[j] && i != j){
                    sol = false;
                    break;
                }
            if(sol)
                return A[i];
        }
        return 0;
    }
    public static void main(String[] args) {
        int[] A = new int[7];
        //input data
        A[0] = 9;  A[1] = 3;  A[2] = 9;
        A[3] = 3;  A[4] = 3;  A[5] = 7;
        A[6] = 9;
        //display solution
        int result = solution(A);
        if(result==7)
            System.out.println("The result it's corect.");
        else
            System.out.println("The result it's incorect.");
    }
}
