package g30126.Valcelean.Cristina.l3.e5;
/*Every morning karel is awakened when the newspaper, represented by a Thing,
is thrown on the front porch of its house. Instruct karel to retrieve the newspaper
and return to �bed.� The initial situation is as shown in Figure 1-29; in the
final situation, karel is on its original intersection, facing its original direction,
with the newspaper.*/

import becker.robots.*;

public class Ex5 {

        public static void main (String[] args) {
            City ny = new City();
            Thing parcel = new Thing(ny, 2, 2);
            Wall o0 = new Wall(ny, 1, 1, Direction.WEST);
            Wall o1 = new Wall(ny, 2, 1, Direction.WEST);
            Wall o2 = new Wall(ny, 1, 2, Direction.EAST);
            Wall o3 = new Wall(ny, 1, 1, Direction.NORTH);
            Wall o4 = new Wall(ny, 1, 2, Direction.NORTH);
            Wall o5 = new Wall(ny, 2, 1, Direction.SOUTH);
            Wall o6 = new Wall(ny, 1, 2, Direction.SOUTH);
            Robot karel = new Robot(ny, 1, 2, Direction.SOUTH);

            // Direct the robot to the final situation
            karel.turnLeft();
            karel.turnLeft();
            karel.turnLeft();
            karel.move();
            karel.turnLeft();
            karel.move();
            karel.turnLeft();
            karel.move();
            karel.pickThing();
            karel.turnLeft();
            karel.turnLeft();
            karel.move();
            karel.turnLeft();
            karel.turnLeft();
            karel.turnLeft();
            karel.move();
            karel.turnLeft();
            karel.turnLeft();
            karel.turnLeft();
            karel.move();
            karel.turnLeft();
            karel.turnLeft();
            karel.turnLeft();
        }

    }



