package g30126.Valcelean.Cristina.l3.e3;

//Write a program that creates a robot at (1, 1) that moves north five times, turns around, and returns to its starting point.

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;

public class Ex3 {
    public static void main (String[] args) {
        // Set up the initial situation
        City cluj = new City();
        Robot mark = new Robot(cluj, 1, 1, Direction.NORTH);

        // Direct the robot to the final situation
        mark.move();
        mark.move();
        mark.move();
        mark.move();
        mark.move();
        mark.turnLeft();
        mark.turnLeft();
        mark.move();
        mark.move();
        mark.move();
        mark.move();
        mark.move();
    }
}
