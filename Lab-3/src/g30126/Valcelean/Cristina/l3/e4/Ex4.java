package g30126.Valcelean.Cristina.l3.e4;

/*Write a program that begins with the initial situation shown in Figure 1-28.
Instruct the robot to go around the walls counter clockwise and return to its
starting position. Hint: Setting up the initial situation requires eight walls.*/

import becker.robots.*;

public class Ex4 {
    public static void main (String[] args) {
        City ny = new City();
        Wall v0 = new Wall(ny, 1, 1, Direction.WEST);
        Wall v1 = new Wall(ny, 2, 1, Direction.WEST);
        Wall v2 = new Wall(ny, 1, 2, Direction.EAST);
        Wall v3 = new Wall(ny, 2, 2, Direction.EAST);
        Wall v4 = new Wall(ny, 1, 1, Direction.NORTH);
        Wall v5 = new Wall(ny, 1, 2, Direction.NORTH);
        Wall v6 = new Wall(ny, 2, 1, Direction.SOUTH);
        Wall v7 = new Wall(ny, 2, 2, Direction.SOUTH);
        Robot mark = new Robot(ny, 0, 2, Direction.WEST);

        // Direct the robot to the final situation
        mark.move();
        mark.move();
        mark.turnLeft();	// start turning right as three turn lefts
        mark.move();
        mark.move();
        mark.move();
        mark.turnLeft();
        mark.move();
        mark.move();
        mark.move();
        mark.turnLeft();
        mark.move();
        mark.move();
        mark.move();
        mark.turnLeft();
        mark.move();
    }

}
