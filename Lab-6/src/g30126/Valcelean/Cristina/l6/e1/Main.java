package g30126.Valcelean.Cristina.l6.e1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1= new Rectangle(Color.blue,100,"ABC433ER",true,85,63,80,55);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.RED, 15, "Cerc1", false,120,60,140,80);
        b1.addShape(s2);
        Shape s3 = new Circle(Color.GREEN, 80,"Cerc2",true,70,95,60,90);
        b1.addShape(s3);
        b1.deleteById("Cerc1");
    }
}