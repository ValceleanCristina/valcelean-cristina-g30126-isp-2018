package g30126.Valcelean.Cristina.l6.e1;

import java.awt.*;

public class Circle extends Shape {

    private int radius;
    // attributes a and b are coordinated to place the id of the circle
    private int a;
    private int b;

    public Circle(Color color, int radius, String id, boolean fill, int x, int y, int a, int b) {
        super(color,fill,id,x,y);
        this.radius = radius;
        this.a=a;
        this.b=b;
    }

    public int getRadius() {
        return radius;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle " + this.radius + " " + getColor().toString()+ "with id "+getId());
        g.setColor(getColor());
        g.drawOval(getX(), getY(), getRadius(), getRadius());

        g.drawChars(getId().toCharArray(), 0,getId().length(), getA(), getB());  //display the id of the figure

        if (isFill()==true)
            g.fillOval(this.getX(),this.getY(),this.getRadius(),this.getRadius());
    }
}

