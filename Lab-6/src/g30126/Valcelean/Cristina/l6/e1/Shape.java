package g30126.Valcelean.Cristina.l6.e1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    public String id;
    public int x;
    public int y;
    private boolean fill;

    public Shape(Color color, boolean fill,String id, int x, int y) {
        this.color = color;
        this.fill=fill;
        this.id=id;
        this.x=x;
        this.y=y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isFill() {
        return fill;
    }

    public void setFill(boolean fill) {
        this.fill = fill;
    }

    public abstract void draw(Graphics g);
}