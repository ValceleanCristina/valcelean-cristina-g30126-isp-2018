package g30126.Valcelean.Cristina.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    // attributes a and b are coordinated to place the id of the rectangle
    private int a;
    private int b;

    public Rectangle(Color color, int length, String id, boolean fill, int x, int y, int a, int b ){
        super(color, fill, id, x, y);
        this.length = length;
        this.a=a;
        this.b=b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString()+ "with id "+getId());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,length);

        g.drawChars(getId().toCharArray(),0,getId().length(), getA(), getB());  //display the id of the figure

        if (isFill()==true)
            g.fillRect(this.getX(),this.getY(),150,200);
    }
}