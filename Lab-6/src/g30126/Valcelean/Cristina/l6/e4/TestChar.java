package g30126.Valcelean.Cristina.l6.e4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestChar {
    @Test
    public void test1(){
        CharSeqImplem s = new CharSeqImplem("a123");
        assertEquals(s.length(), 4);
    }
    @Test
    public void test2(){
        CharSeqImplem s = new CharSeqImplem("bc243");
        assertEquals(s.charAt(3),'4');
    }
    @Test
    public void test3(){
        CharSeqImplem s = new CharSeqImplem("can243");
        assertEquals(s.subSequence(2,4).toString(), "n2");
    }
}
