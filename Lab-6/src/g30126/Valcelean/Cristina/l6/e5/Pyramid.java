package g30126.Valcelean.Cristina.l6.e5;

import java.awt.*;
import acm.program.*;
import acm.graphics.*;

public class Pyramid extends GraphicsProgram{

    private   int width = 30;
    private   int height = 12;
    private   int bricks_base = 14;
    DrawingBoard b1 = new DrawingBoard();

    public Pyramid(int width, int height) {
        this.height = height;
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getBricks() {
        return bricks_base;
    }

    public void run() {
        for (int i = 0; i < bricks_base; i++) {
            for (int j= 0;j<i; j++) {
                int x = width*j;
                int y = height*i;

                Bricks s1 = new Bricks(x, y, this.width, this.height, Color.BLUE, true);
                b1.addBricks(s1);
            }
        }
    }
}
