package g30126.Valcelean.Cristina.l6.e5;

import java.awt.*;

public class Bricks {

        private int x, y, width, height;
        private Color color;
        private boolean fill;

        public Bricks(int x, int y, int width, int height, Color color, boolean fill) {
            this.color=color;
            this.fill=fill;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public int getY() {
            return y;
        }

        public int getHeight() {
            return height;
        }

        public int getX() {
            return x;
        }

        public int getWidth() {
            return width;
        }

        public void draw(Graphics g) {
            System.out.println("Brick with w=" + getWidth() + " h= " + getHeight());
            g.drawRect(getX(), getY(), getHeight(), getWidth());
        }
}

