package g30126.Valcelean.Cristina.l6.e5;

import javax.swing.*;
import java.awt.*;

public class DrawingBoard extends JFrame{
    Bricks[] bricks = new Bricks[10];

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addBricks(Bricks s1){
        for(int i=0;i<bricks.length;i++){
            if(bricks[i]==null){
                bricks[i] = s1;
                break;
            }
        }
//        shapes.add(s1);
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<bricks.length;i++){
            if(bricks[i]!=null)
                bricks[i].draw(g);
        }
    }
}
