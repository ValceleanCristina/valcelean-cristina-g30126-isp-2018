package g30126.Valcelean.Cristina.l6.e3;

import java.awt.*;

public interface Shape {
    void draw(Graphics g);
    String getId();
}
