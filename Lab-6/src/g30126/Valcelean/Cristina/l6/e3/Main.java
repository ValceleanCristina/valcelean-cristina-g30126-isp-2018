package g30126.Valcelean.Cristina.l6.e3;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1= new Rectangle(Color.blue,100,"ABC433ER",true,85,50,80,45);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.RED, 200, 140, true,"a123",60,190,130);
        b1.addShape(s2);
        Shape s3 = new Circle(Color.GREEN, 100,80,false,"e245",55,95,70);
        b1.addShape(s3);
       // b1.deleteById("e245");
    }
}
