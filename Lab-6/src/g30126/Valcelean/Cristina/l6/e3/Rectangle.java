package g30126.Valcelean.Cristina.l6.e3;

import java.awt.*;

public class Rectangle implements Shape {

    private int length;
    private int x,y,a,b;
    private Color color;
    private boolean fill;
    private String id;


    public Rectangle(Color color, int length, String id, boolean fill, int x, int y, int a, int b) {
        this.color = color;
        this.length = length;
        this.id = id;
        this.x = x;
        this.y = y;
        this.fill = fill;
        this.a=a;
        this.b=b;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Color getColor() {
        return color;
    }

    public boolean isFill() {
        return fill;
    }

    public String getId() {
        return id;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel " + length + " " + getColor().toString() + "with id " + getId());
        g.setColor(getColor());
        g.drawRect(getX(), getY(), length, length);

        g.drawChars(getId().toCharArray(), 0, getId().length(), getA(), getB());  //afiseaza id-ul fiecarei figuri

        if (isFill())
            g.fillRect(getX(), getY(), 150, 200);


    }

}