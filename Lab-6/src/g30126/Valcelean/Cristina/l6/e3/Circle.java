package g30126.Valcelean.Cristina.l6.e3;

import java.awt.*;

public class Circle implements  Shape {

    private Color color;
    private int x, y,a,b;
    private int radius;
    private String id;
    private boolean fill;

    public Circle(Color color, int x, int y, boolean fill, String id, int radius,int a, int b) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.fill = fill;
        this.id = id;
        this.radius = radius;
        this.a=a;
        this.b=b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public Color getColor() {
        return color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getId() {
        return id;
    }

    public boolean isFill() {
        return fill;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle " + this.radius + " " + getColor().toString()+ "with id "+getId());
        g.setColor(getColor());
        g.drawOval(getX(), getY(), radius, radius);

        g.drawChars(getId().toCharArray(), 0, getId().length(), getA(), getB());  //afiseaza id-ul fiecarei figuri

        if (isFill() == true)
            g.fillOval(getX(), getY(), radius, radius);
    }
}
